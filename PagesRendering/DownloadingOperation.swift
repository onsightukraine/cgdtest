//
//  DownloadingOperation.swift
//  PagesRendering
//
//  Created by Lera Mozgovaya on 1/28/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

enum PhotoOperationState {
    case new, downoaded, failed
}

class DownloadingOperation: Operation {
    
    let photoRecord: PhotoRecord
    
    init(photoRecord: PhotoRecord) {
        self.photoRecord = photoRecord
    }
    
    override func main() {
        
        if isCancelled {
            return
        }
        
        guard let imageData = try? Data(contentsOf: photoRecord.url) else { return }
        
        if isCancelled {
            return
        }
        
        if let image = UIImage(data: imageData) {
            photoRecord.image = image
            photoRecord.state = .downoaded
        }
        
        else {
            photoRecord.state = .failed
            photoRecord.image = nil
        }
    }
}

class PendingOperations {
    
    lazy var downloadInProgress: [IndexPath: Operation] = [:]
    
    lazy var downloadQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Download queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
}
