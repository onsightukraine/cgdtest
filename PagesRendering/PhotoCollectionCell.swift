//
//  PhotoCollectionCell.swift
//  PagesRendering
//
//  Created by Lera Mozgovaya on 1/28/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView! {
        didSet {
            backgroundColor = UIColor(red: 0.1, green: 0.2, blue: 0.3, alpha: 0.5)
        }
    }
    
    @IBOutlet weak var label: UILabel!
    
    var photo: PhotoRecord? {
        didSet {
            imgView.image = photo?.image
            label.text = photo?.name
        }
    }
}
