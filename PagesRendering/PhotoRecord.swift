//
//  PhotoRecord.swift
//  PagesRendering
//
//  Created by Lera Mozgovaya on 1/28/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import Foundation
import UIKit

class PhotoRecord {
    
    let name: String
    let url: URL
    var state = PhotoOperationState.new
    var image = UIImage(named: "Placeholder")
    
    init(name: String, url: URL) {
        self.name = name
        self.url  = url
    }
}
